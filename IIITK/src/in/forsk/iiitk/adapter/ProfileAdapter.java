package in.forsk.iiitk.adapter;

/**
 * Created by Timus on 3/11/2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

//ArrayAdapter - A concrete BaseAdapter that is backed by an array of arbitrary objects(wrappers mostly).
public class ProfileAdapter extends BaseAdapter {

    private final static String TAG = ProfileAdapter.class.getSimpleName();

    Context context;
    int resource;
    ArrayList<profileWrapper> mStudentDataList;

    LayoutInflater inflater;

    AQuery aq;

    public ProfileAdapter(Context context, ArrayList<profileWrapper> mStudentDataList) {

        this.context = context;
        this.mStudentDataList = mStudentDataList;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        aq = new AQuery(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHoder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.student_row, null);
            holder	=	new ViewHoder(convertView);

            convertView.setTag(holder);

        } else {
            holder	=	(ViewHoder) convertView.getTag();

        }

        // Retrieve the data at particular index(position)
        // so we can bind the correct data
        profileWrapper obj = mStudentDataList.get(position);

        AQuery temp_aq = aq.recycle(convertView);
        temp_aq.id(holder.profileIv).image(obj.getPhoto(), true, true, 200, 0);
     //   mStudentDataList.get(position).setPic(temp_aq.id(holder.profileIv).image(obj.getPhoto(), true, true, 200, 0).);
        /*
    * IMAGE ROUND
    * */
        //Bitmap bm = BitmapFactory.decodeResource(Resources.getSystem(),holder.profileIv.getId());
             //   decodeResource(holder.profileIv.getResources());

       // holder.profileIv.setImageBitmap(getCircleBitmap(bm));
        /*
    * IMAGE ROUND
    * */

        holder.nameTv.setText(obj.getFirst_name() + " "+obj.getMiddle_name()+" " + obj.getLast_name());
        holder.departmentTv.setText(obj.getDepartment());
        holder.collegeTv.setText(obj.getCollegeid());

        return convertView;
    }

    /*
    * IMAGE ROUND
    */

/*
    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }*/


    /*
    * IMAGE ROUND
    * */

    public static class ViewHoder {
        ImageView profileIv;
        TextView nameTv, departmentTv, collegeTv;

        public ViewHoder(View view) {
            profileIv = (ImageView) view.findViewById(R.id.image);

            nameTv = (TextView) view.findViewById(R.id.Name);
            collegeTv = (TextView) view.findViewById(R.id.CollegeId);
            departmentTv = (TextView) view.findViewById(R.id.Department);
        }
    }

    // These are the 3 new methods we need to override in base adapter
    @Override
    public int getCount() { return mStudentDataList.size(); }

    @Override
    public Object getItem(int position) { return mStudentDataList.get(position); }

    @Override
    public long getItemId(int position) { return position; }

}
