package in.forsk.iiitk;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class StudentList extends Activity {
    private static final String TAG = "Sumit" ;
    Context context;
    ArrayList<profileWrapper> mStudentDataList;
    String response="";
    ProfileAdapter mAdapter;
    ListView StudentList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);
        context=this;
        new CustomAsyncTask().execute();
        StudentList=(ListView)findViewById(R.id.StudentList);
        StudentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StudentDetail.name=mStudentDataList.get(position).getFirst_name()+" "+mStudentDataList.get(position).getMiddle_name()+" "+mStudentDataList.get(position).getLast_name();
                StudentDetail.collegeid= mStudentDataList.get(position).getCollegeid();
                StudentDetail.department=mStudentDataList.get(position).getDepartment();
                StudentDetail.date_of_birth=mStudentDataList.get(position).getDate_of_birth();
                StudentDetail.batch=mStudentDataList.get(position).getBatch();
                StudentDetail.current_address=mStudentDataList.get(position).getCurrent_address();
                StudentDetail.hometown=mStudentDataList.get(position).getHometown();
                StudentDetail.state=mStudentDataList.get(position).getState();
                StudentDetail.country=mStudentDataList.get(position).getCountry();
                StudentDetail.email=mStudentDataList.get(position).getEmail();
                StudentDetail.phone=mStudentDataList.get(position).getPhone();
                StudentDetail.photo=mStudentDataList.get(position).getPhoto();


                Intent next = new Intent(getBaseContext(),StudentDetail.class);
                startActivity(next);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String openHttpConnection(String urlStr) throws IOException {
        InputStream in = null;
        int resCode = -1;

        try {
            URL url = new URL("http://online.mnit.ac.in/iiitk/assets/studentprofile.json");
            URLConnection urlConn = url.openConnection();

            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("Page Not Found");
            }

            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            resCode = httpConn.getResponseCode();
            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
                System.out.println("x"+in);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return convertStreamToString(in);
    }

    private String convertStreamToString(InputStream is) throws IOException {
        // Converting input stream into string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();

        while (i != -1) {
            baos.write(i);
            i = is.read();

        }
        return baos.toString();
    }

    // Params, Progress, Result
    class CustomAsyncTask extends AsyncTask<Void, Void, String> {

        String url;
        String result = "";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            Toast.makeText(context, "Student List is Loading...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                result = openHttpConnection(url);

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
          response=result;
            mStudentDataList= parseStudentFile(response);
            setStudentListAdapter(mStudentDataList);
            Toast.makeText(context, "Student List has been Loaded", Toast.LENGTH_SHORT).show();
        }


    }
    public void setStudentListAdapter(ArrayList<profileWrapper> mStudentDataList) {
        mAdapter = new ProfileAdapter(context, mStudentDataList);
        StudentList.setAdapter(mAdapter);
    }

    public ArrayList<profileWrapper> parseStudentFile(String json_string) {

        ArrayList<profileWrapper> mStudentDataList = new ArrayList<>();
        try {
            // Converting multipal json data (String) into Json array
            JSONArray StudentArray = new JSONArray(json_string);
            Log.d(TAG, StudentArray.toString());
            // Iterating json array into json objects
            for (int i = 0; StudentArray.length() > i; i++) {

                // Extracting json object from particular index of array
                JSONObject StudentJsonObject = StudentArray.getJSONObject(i);

                // Design patterns
                profileWrapper StudentObject = new profileWrapper(StudentJsonObject);



                mStudentDataList.add(StudentObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mStudentDataList;
    }
}