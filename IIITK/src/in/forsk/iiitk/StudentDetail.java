package in.forsk.iiitk;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

public class StudentDetail extends Activity {
    static String header="";
    static String photo="";
    static String name="";
    static String collegeid="";
    static String department="";
    static String date_of_birth="";
    static String batch="";
    static String current_address="";
    static String hometown="";
    static String state="";
    static String country="";
    static String email="";
    static String phone="";
    TextView view_header,view_name,view_collegeid,view_batch,view_date_of_birth,view_current_address,view_department,view_hometown,view_country,view_state,view_email,view_phone;
    Bitmap bm;
    ImageView view_photo;
    AQuery aq;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_detail);

        aq = new AQuery(context);

        view_name = (TextView)findViewById(R.id.Name);
        view_header = (TextView)findViewById(R.id.header);
        view_collegeid = (TextView)findViewById(R.id.CollegeId);
        view_department = (TextView)findViewById(R.id.Course);
        view_date_of_birth = (TextView)findViewById(R.id.DateOfBirth);
        view_batch = (TextView)findViewById(R.id.Batch);
        view_current_address = (TextView)findViewById(R.id.Address);
        view_hometown = (TextView)findViewById(R.id.Hometown);
        view_state = (TextView)findViewById(R.id.State);
        view_country = (TextView)findViewById(R.id.Country);
        view_email = (TextView)findViewById(R.id.Email);
        view_phone = (TextView)findViewById(R.id.Phone);
        view_photo = (ImageView)findViewById(R.id.profilePic);

        //view_photo.setImageBitmap(bm);
        view_header.setText(name);
        view_name.setText(name);
        view_collegeid.setText(collegeid);
        view_department.setText(department);
        view_date_of_birth.setText(date_of_birth);
        view_batch.setText(batch);
        view_current_address.setText(current_address);
        view_hometown.setText(hometown);
        view_state.setText(state);
        view_country.setText(country);
        view_email.setText(email);
        view_phone.setText(phone);

        AQuery aq = new AQuery(getApplicationContext());
        aq.id(R.id.profilePic).image(photo,true,true, 0, R.drawable.ic_launcher );

    }

}
